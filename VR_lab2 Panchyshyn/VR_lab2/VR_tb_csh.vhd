-- Vhdl test bench created from schematic D:\XI_LABS\VR_lab2\VR_sch.sch - Thu Nov 30 12:51:44 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY VR_sch_VR_sch_sch_tb IS
END VR_sch_VR_sch_sch_tb;
ARCHITECTURE behavioral OF VR_sch_VR_sch_sch_tb IS 

   COMPONENT VR_sch
   PORT( VR_c_m	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          VR_c_mm	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          VR_c_ip	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          VR_a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          VR_b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0));
   END COMPONENT;

   SIGNAL VR_c_m	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL VR_c_mm	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL VR_c_ip	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL VR_a	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"98";
   SIGNAL VR_b	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"71";

BEGIN

   UUT: VR_sch PORT MAP(
		VR_c_m => VR_c_m, 
		VR_c_mm => VR_c_mm, 
		VR_c_ip => VR_c_ip, 
		VR_a => VR_a, 
		VR_b => VR_b
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		VR_a <= x"00" after 10 ns;
		VR_b <= x"43" after 10 ns;
      WAIT for 10 ns; 
		VR_a <= x"FF" after 10 ns;
		VR_b <= x"FF" after 10 ns;
      WAIT for 10 ns; 
		VR_a <= x"56" after 10 ns;
		VR_b <= x"01" after 10 ns;
      WAIT for 10 ns; 
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
