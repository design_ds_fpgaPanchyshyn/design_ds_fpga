<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KP_A(7:0)" />
        <signal name="KP_Y(15:0)" />
        <signal name="KP_Y_ip(16:0)" />
        <signal name="KP_CE" />
        <signal name="KP_CLR" />
        <signal name="KP_CLK" />
        <signal name="XLXN_2" />
        <signal name="XLXN_4(7:0)" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6(7:0)" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8(7:0)" />
        <signal name="XLXN_9" />
        <signal name="rfd" />
        <signal name="rdy" />
        <port polarity="Input" name="KP_A(7:0)" />
        <port polarity="Output" name="KP_Y(15:0)" />
        <port polarity="Output" name="KP_Y_ip(16:0)" />
        <port polarity="Input" name="KP_CE" />
        <port polarity="Input" name="KP_CLR" />
        <port polarity="Input" name="KP_CLK" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="rdy" />
        <blockdef name="KP_fir_sh">
            <timestamp>2017-12-13T14:38:40</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="KP_IP_fir">
            <timestamp>2017-12-14T11:57:35</timestamp>
            <rect width="512" x="32" y="32" height="216" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="32" y1="160" y2="160" x1="0" />
            <line x2="544" y1="144" y2="144" x1="576" />
            <line x2="544" y1="176" y2="176" x1="576" />
        </blockdef>
        <block symbolname="KP_fir_sh" name="XLXI_1">
            <blockpin signalname="KP_A(7:0)" name="KP_A(7:0)" />
            <blockpin signalname="KP_CE" name="KP_CE" />
            <blockpin signalname="KP_CLK" name="KP_CLK" />
            <blockpin signalname="KP_CLR" name="KP_CLR" />
            <blockpin signalname="KP_Y(15:0)" name="KP_Y(15:0)" />
        </block>
        <block symbolname="KP_IP_fir" name="XLXI_2">
            <blockpin signalname="KP_A(7:0)" name="din(7:0)" />
            <blockpin signalname="KP_Y_ip(16:0)" name="dout(16:0)" />
            <blockpin signalname="KP_CLK" name="clk" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="448" y="496" name="XLXI_1" orien="R0">
        </instance>
        <branch name="KP_A(7:0)">
            <wire x2="320" y1="272" y2="272" x1="256" />
            <wire x2="448" y1="272" y2="272" x1="320" />
            <wire x2="320" y1="272" y2="640" x1="320" />
            <wire x2="480" y1="640" y2="640" x1="320" />
        </branch>
        <branch name="KP_Y(15:0)">
            <wire x2="1216" y1="272" y2="272" x1="832" />
        </branch>
        <branch name="KP_Y_ip(16:0)">
            <wire x2="1232" y1="640" y2="640" x1="1056" />
        </branch>
        <branch name="KP_CE">
            <wire x2="448" y1="336" y2="336" x1="256" />
        </branch>
        <branch name="KP_CLR">
            <wire x2="448" y1="464" y2="464" x1="256" />
        </branch>
        <iomarker fontsize="28" x="256" y="272" name="KP_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="256" y="336" name="KP_CE" orien="R180" />
        <iomarker fontsize="28" x="256" y="400" name="KP_CLK" orien="R180" />
        <iomarker fontsize="28" x="256" y="464" name="KP_CLR" orien="R180" />
        <iomarker fontsize="28" x="1216" y="272" name="KP_Y(15:0)" orien="R0" />
        <iomarker fontsize="28" x="1232" y="640" name="KP_Y_ip(16:0)" orien="R0" />
        <branch name="KP_CLK">
            <wire x2="384" y1="400" y2="400" x1="256" />
            <wire x2="448" y1="400" y2="400" x1="384" />
            <wire x2="384" y1="400" y2="720" x1="384" />
            <wire x2="480" y1="720" y2="720" x1="384" />
        </branch>
        <iomarker fontsize="28" x="1136" y="704" name="rfd" orien="R0" />
        <instance x="480" y="560" name="XLXI_2" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1136" y="736" name="rdy" orien="R0" />
        <branch name="rfd">
            <wire x2="1136" y1="704" y2="704" x1="1056" />
        </branch>
        <branch name="rdy">
            <wire x2="1136" y1="736" y2="736" x1="1056" />
        </branch>
    </sheet>
</drawing>