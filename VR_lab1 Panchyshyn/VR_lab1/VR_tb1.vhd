-- Vhdl test bench created from schematic D:\XI_LABS\VR_lab1\VR_sch.sch - Thu Nov 30 11:05:06 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY VR_sch_VR_sch_sch_tb IS
END VR_sch_VR_sch_sch_tb;
ARCHITECTURE behavioral OF VR_sch_VR_sch_sch_tb IS 

   COMPONENT VR_sch
   PORT( VR_c	:	IN	STD_LOGIC; 
          VR_a	:	IN	STD_LOGIC; 
          VR_d	:	IN	STD_LOGIC; 
          VR_e	:	IN	STD_LOGIC; 
          VR_b	:	IN	STD_LOGIC; 
          VR_f	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL VR_c	:	STD_LOGIC :='0';
   SIGNAL VR_a	:	STD_LOGIC :='0';
   SIGNAL VR_d	:	STD_LOGIC :='0';
   SIGNAL VR_e	:	STD_LOGIC :='0';
   SIGNAL VR_b	:	STD_LOGIC :='0';
   SIGNAL VR_f	:	STD_LOGIC :='0';

BEGIN

   UUT: VR_sch PORT MAP(
		VR_c => VR_c, 
		VR_a => VR_a, 
		VR_d => VR_d, 
		VR_e => VR_e, 
		VR_b => VR_b, 
		VR_f => VR_f
   );
	VR_a <= not VR_a after 160 ns;
	VR_b <= not VR_b after 80 ns;
	VR_c <= not VR_c after 40 ns;
	VR_d <= not VR_d after 20 ns;
	VR_e <= not VR_e after 10 ns;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
