/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/XI_LABS/KP_lab4/KP_conv_tb.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );


static void work_a_3128948588_3212880686_p_0(char *t0)
{
    int64 t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(71, ng0);

LAB3:    t1 = (10 * 1000LL);
    t2 = (t0 + 1832U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 5256);
    t6 = (t2 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t5;
    xsi_driver_first_trans_delta(t2, 0U, 1, t1);
    t10 = (t0 + 5256);
    xsi_driver_intertial_reject(t10, t1, t1);

LAB2:    t11 = (t0 + 5176);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3128948588_3212880686_p_1(char *t0)
{
    int64 t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(72, ng0);

LAB3:    t1 = (100 * 1000LL);
    t2 = (t0 + 5320);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_delta(t2, 0U, 1, t1);
    t7 = (t0 + 5320);
    xsi_driver_intertial_reject(t7, t1, t1);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3128948588_3212880686_p_2(char *t0)
{
    int64 t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(73, ng0);

LAB3:    t1 = (150 * 1000LL);
    t2 = (t0 + 5384);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_delta(t2, 0U, 1, t1);
    t7 = (t0 + 5384);
    xsi_driver_intertial_reject(t7, t1, t1);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3128948588_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    t1 = (t0 + 4856U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(77, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8247);
    t5 = (t0 + 5448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5448);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(78, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8263);
    t5 = (t0 + 5512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5512);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(79, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8279);
    t5 = (t0 + 5576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5576);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(80, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8295);
    t5 = (t0 + 5640);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5640);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(81, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(83, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8311);
    t5 = (t0 + 5448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5448);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(84, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8327);
    t5 = (t0 + 5512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5512);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(85, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8343);
    t5 = (t0 + 5576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5576);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(86, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8359);
    t5 = (t0 + 5640);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5640);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(87, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t3);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    xsi_set_current_line(90, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8375);
    t5 = (t0 + 5448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5448);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(91, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8391);
    t5 = (t0 + 5512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5512);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(92, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8407);
    t5 = (t0 + 5576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5576);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(93, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8423);
    t5 = (t0 + 5640);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5640);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(94, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t3);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    xsi_set_current_line(96, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8439);
    t5 = (t0 + 5448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5448);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(97, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8455);
    t5 = (t0 + 5512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5512);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(98, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8471);
    t5 = (t0 + 5576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5576);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(99, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8487);
    t5 = (t0 + 5640);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5640);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(100, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t3);

LAB18:    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    xsi_set_current_line(102, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8503);
    t5 = (t0 + 5448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5448);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(103, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8519);
    t5 = (t0 + 5512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5512);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(104, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8535);
    t5 = (t0 + 5576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5576);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(105, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8551);
    t5 = (t0 + 5640);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5640);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(106, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t3);

LAB22:    *((char **)t1) = &&LAB23;
    goto LAB1;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

LAB20:    xsi_set_current_line(108, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8567);
    t5 = (t0 + 5448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5448);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(109, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8583);
    t5 = (t0 + 5512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5512);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(110, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8599);
    t5 = (t0 + 5576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5576);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(111, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 8615);
    t5 = (t0 + 5640);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 5640);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(112, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t3);

LAB26:    *((char **)t1) = &&LAB27;
    goto LAB1;

LAB21:    goto LAB20;

LAB23:    goto LAB21;

LAB24:    goto LAB2;

LAB25:    goto LAB24;

LAB27:    goto LAB25;

}


extern void work_a_3128948588_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3128948588_3212880686_p_0,(void *)work_a_3128948588_3212880686_p_1,(void *)work_a_3128948588_3212880686_p_2,(void *)work_a_3128948588_3212880686_p_3};
	xsi_register_didat("work_a_3128948588_3212880686", "isim/KP_conv_sh_KP_conv_sh_sch_tb_isim_beh.exe.sim/work/a_3128948588_3212880686.didat");
	xsi_register_executes(pe);
}
