/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/XI_LABS/vr_lab3/vr_tb.vhd";



static void work_a_2217056900_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    t1 = (t0 + 2512U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(45, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 4652);
    t5 = (t0 + 2896);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 2896);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(46, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 2320);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(47, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 4668);
    t5 = (t0 + 2896);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 2896);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(48, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 2320);
    xsi_process_wait(t2, t3);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    xsi_set_current_line(49, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 4684);
    t5 = (t0 + 2896);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 2896);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(50, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 2320);
    xsi_process_wait(t2, t3);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    xsi_set_current_line(51, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 4700);
    t5 = (t0 + 2896);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 2896);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(52, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 2320);
    xsi_process_wait(t2, t3);

LAB18:    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    goto LAB2;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

}


extern void work_a_2217056900_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2217056900_3212880686_p_0};
	xsi_register_didat("work_a_2217056900_3212880686", "isim/vr_sh_vr_sh_sch_tb_isim_beh.exe.sim/work/a_2217056900_3212880686.didat");
	xsi_register_executes(pe);
}
